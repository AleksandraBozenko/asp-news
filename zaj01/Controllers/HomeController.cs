﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Xml.Linq;
using zaj01.Models;

namespace zaj01.Controllers
{
    public class PaginationViewModel
    {
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public int TotalItems { get; set; }
        public int TotalPages { get; set; }
    }

    public class NewsViewModel
    {
        public List<RssItem> News { get; set; }
        public PaginationViewModel Pagination { get; set; }
    }
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult DisplayNews(int pageSize = 4, int page = 1)
        {
            var rrs = XElement.Load("https://news.google.com/rss?topic=h&hl=pl&gl=PL&ceid=PL:pl");
            var news = rrs.Descendants("item").Select(x => new RssItem
            {
                Title = x.Element("title")?.Value,
                PubDate = x.Element("pubDate")?.Value,
                Description = x.Element("description")?.Value,
            });
            int totalItems = news.Count();
            int totalPages = (int)Math.Ceiling((double)totalItems / pageSize);
            int currentPage = page > totalPages ? totalPages : page;
            int skip = (currentPage - 1) * pageSize;
            var pagedNews = news.Skip(skip).Take(pageSize).ToList();
            var model = new NewsViewModel
            {
                News = pagedNews,
                Pagination = new PaginationViewModel
                {
                    CurrentPage = currentPage,
                    PageSize = pageSize,
                    TotalItems = totalItems,
                    TotalPages = totalPages
                }
            };
            return View(model);
        }




        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}