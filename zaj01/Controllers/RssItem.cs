﻿namespace zaj01.Controllers
{
    public class RssItem
    {
        public string? Title { get; set; }
        public string? PubDate { get; set; }
        public string? Description { get; set; }
    }
}